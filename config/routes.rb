Rails.application.routes.draw do
  resources :reviews
  resources :movies
  get "up" => "rails/health#show", as: :rails_health_check
end
