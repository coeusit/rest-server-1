require_relative "boot"
require "rails/all"

Bundler.require(*Rails.groups)

module RestServer
  class Application < Rails::Application
    config.load_defaults 7.1
    config.autoload_lib(ignore: %w(assets tasks))
    config.autoload_paths << "#{Rails.root}/lib"
    config.api_only = true
  end
end
