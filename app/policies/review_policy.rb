class ReviewPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.joins(movie: :api_key).where(movies: { api_key_id: api_key.id })
    end
  end
end
