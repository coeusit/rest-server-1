class MoviePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.where(api_key_id: api_key.id)
    end
  end
end
