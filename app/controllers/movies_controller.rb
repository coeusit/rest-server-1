class MoviesController < ResourceController
    key :uuid
    model 'Movie'
    viewable_fields :uuid, :title, :description
    writable_fields :title, :description

    def validates(form)
        form[:api_key_id] = pundit_user.id
        form
    end
end
