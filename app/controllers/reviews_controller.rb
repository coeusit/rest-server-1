class ReviewsController < ResourceController
    key :uuid
    model 'Review'
    viewable_fields :uuid, :movie_id, :author, :content, :rating
    writable_fields :movie_id, :author, :content, :rating
  
    def validates(form)
        if policy_scope(Movie).find_by(id: form[:movie_id]).nil?
            false
        else
            form
        end
    end
  end
  