class ResourceController < ApplicationController
    include Pundit::Authorization
    before_action :verify_authentication_header

    @@key, @@model, @@viewable_fields, @@writable_fields = *nil

    class << self
        def key(k)
            @@key = k
        end

        def model(m)
            @@model = m
        end
        
        def viewable_fields(*fields)
            @@viewable_fields = fields
        end

        def writable_fields(*fields)
            @@writable_fields = fields
        end
    end

    def create
        params.require(@@model.downcase.to_sym).permit(@@writable_fields)

        form = convert_references_to_id(params[@@model.downcase.to_sym].permit(@@writable_fields))

        unless form.keys.all? { |key| @@writable_fields.include?(key.to_sym) }
            render json: { error: 'Invalid fields in the request' }, status: :bad_request
            return
        end

        form = validates(form)

        if form
            entity = @@model.constantize.create(form.to_h)
            if entity.nil?
                render json: { error: 'An unknown error occurred while creating the entity' }, status: :internal_server_error
            else
                render json: convert_references_to_uuid(entity.slice(@@viewable_fields)), status: :created
            end
        else
            render json: { error: 'Form validation failed' }, status: :bad_request
        end
    end

    def destroy
        entity = fetch_entity

        if entity.nil?
            render json: { error: 'The requested resource cannot be found' }, status: :not_found
        else
            entity.destroy!
            head :no_content
        end
    end

    def index
        collection = fetch_collection

        if collection.empty?
            head :no_content
        else
            result = collection.map { |item| convert_references_to_uuid(item.slice(@@viewable_fields)) }
            render json: result, status: :ok
        end
    end

    def show
        entity = fetch_entity

        if entity.nil?
            render json: { error: 'The requested resource cannot be found' }, status: :not_found
        else
            result = convert_references_to_uuid(entity.slice(@@viewable_fields))
            render json: result, status: :ok
        end
    end

    def update
        entity = fetch_entity

        if entity.nil?
            render json: { error: 'The requested resource cannot be found' }, status: :not_found
            return
        end

        params.require(@@model.downcase.to_sym).permit(@@writable_fields)

        form = convert_references_to_id(params[@@model.downcase.to_sym].permit(@@writable_fields))

        unless form.keys.all? { |key| @@writable_fields.include?(key.to_sym) }
            render json: { error: 'Invalid fields in the request' }, status: :bad_request
            return
        end

        form = validates(form)

        if form
            if entity.update(form.to_h)
                render json: convert_references_to_uuid(entity.slice(@@viewable_fields)), status: :ok
            else
                render json: { error: 'An unknown error occurred while updating the entity' }, status: :internal_server_error
            end
        else
            render json: { error: 'Form validation failed' }, status: :bad_request
        end
    end
    
    def validates(form)
        form
    end

    protected

    def convert_references_to_id(output_params)
        @@model.constantize.reflections.each do |name, reflection|
            if reflection.macro == :belongs_to
                key_field = "#{reflection.name}_id"
                if output_params[key_field]
                    parent_model = reflection.class_name.constantize
                    if parent_model.column_names.include?('uuid')
                        parent = parent_model.find_by(uuid: output_params[key_field])
                        output_params[key_field] = parent&.id
                    end
                end
            end
        end
        output_params
    end
      
    def convert_references_to_uuid(input_params)
        @@model.constantize.reflections.each do |name, reflection|
            if reflection.macro == :belongs_to
                key_field = "#{reflection.name}_id"
                if input_params[key_field]
                    parent_model = reflection.class_name.constantize
                    if parent_model.column_names.include?('uuid')
                        parent = parent_model.find_by(id: input_params[key_field])
                        input_params[key_field] = parent&.uuid
                    end
                end
            end
        end
        input_params
    end

    def fetch_collection
        policy_scope(@@model.constantize)
    end

    def fetch_entity
        policy_scope(@@model.constantize).find_by(@@key => params[:id])
    end

    def pundit_user
        pattern = /^Bearer /
        header = request.headers['Authorization']
        token = header.gsub(pattern, '') if header && header.match(pattern)
        ApiKey.find_by(secret: token)
    end

    def verify_authentication_header
        header = request.headers['Authorization']
        if header.nil?
            render json: { error: 'No credentials were provided' }, status: :forbidden
        end
    end
end