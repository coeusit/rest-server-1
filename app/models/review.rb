class Review < ApplicationRecord
  belongs_to :movie
  has_one :api_key, through: :movie
end
