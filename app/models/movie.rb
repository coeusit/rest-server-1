class Movie < ApplicationRecord
    belongs_to :api_key
    has_many :reviews, dependent: :destroy
end
