class ApiKey < ApplicationRecord
    has_many :movies, dependent: :destroy
    has_many :reviews, through: :movies
end
