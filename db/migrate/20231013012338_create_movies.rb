class CreateMovies < ActiveRecord::Migration[7.1]
  def change
    create_table :movies do |t|
      t.references :api_key, null: false, foreign_key: true
      t.string :uuid
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
