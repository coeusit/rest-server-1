require 'faker'
require 'securerandom'

if ApiKey.all.empty?
    10.times do
        api_key = ApiKey.create(
            name: Faker::Company.unique.name,
            secret: SecureRandom.hex(32)
        )
        30.times do
            movie = api_key.movies.create(
                title: Faker::Movie.title,
                description: Faker::Lorem.paragraphs(number: 3).join("\r\n\r\n")
            )
            15.times do
                movie.reviews.create(
                    author: Faker::Name.first_name,
                    content: Faker::Lorem.paragraph,
                    rating: Faker::Number.within(range: 1..10)
                )
                puts "Created review"
            end
            puts "Prepared movie, title: #{movie.title}"
        end
        puts "Prepared API key, name: #{api_key.name}, secret: #{api_key.secret}"
    end
end